/**
 * Delay method
 * @param {number} ms
 */
export function sleep(ms = 1000) {
    if (typeof ms !== 'number') {
        throw Error('Invalid parameter: ms.')
    }

    return new Promise(resolve => setTimeout(resolve, ms))
}

/**
 * Check if url is valid
 */
export function isUrlValid(url) {
    if (! url) return false

    try {
        new URL(url)

        return true
    } catch {
        return false
    }
}

/**
 * Number to words translator
 * @param {Number} value - number
 * @param {Array} words - array od declinations
 */
export function numberToWords(value, words) {
    const cases = [2, 0, 1, 1, 1, 2] // ru

    value = Math.abs(+value)

    if (typeof words === 'string') {
        words = words.split('_')
    }
    else if (! Array.isArray(words)) {
        throw Error('Invalid parameter: words.')
    }

    return words[(value % 100 > 4 && value % 100 < 20) ? 2 :
        cases[(value % 10 < 5) ? value % 10 : 5]]
}
