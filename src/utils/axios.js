/**
 * Override axios instance
*/
import axios from 'axios'
import tunnel from 'tunnel'

const instance = axios.create({
    baseURL: env.APP_URL,
})

if (env.PROXY_HOST && env.PROXY_PORT) {
    axios.defaults.proxy = false
    axios.defaults.httpsAgent = tunnel.httpsOverHttp({
            proxy: {
                host: env.PROXY_HOST,
                port: env.PROXY_PORT,
            },
        })
}

export default instance
