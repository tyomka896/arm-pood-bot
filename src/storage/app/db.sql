--
-- File generated with SQLiteStudio v3.4.3 on Tue Jun 20 17:49:48 2023
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: session
DROP TABLE IF EXISTS session;

CREATE TABLE IF NOT EXISTS session (
    user_id    INTEGER NOT NULL
                       CONSTRAINT session_user_id REFERENCES user (id) ON DELETE CASCADE
                                                                       ON UPDATE CASCADE,
    chat_id    INTEGER NOT NULL,
    value      TEXT    NOT NULL
                       DEFAULT ('{}'),
    created_at TEXT    NOT NULL,
    updated_at TEXT    NOT NULL,
    CONSTRAINT session_user_chat_id PRIMARY KEY (
        user_id,
        chat_id
    )
);


-- Table: user
DROP TABLE IF EXISTS user;

CREATE TABLE IF NOT EXISTS user (
    id         INTEGER PRIMARY KEY
                       UNIQUE
                       NOT NULL,
    utc        INTEGER NOT NULL
                       DEFAULT (7),
    role       TEXT    NOT NULL
                       DEFAULT none,
    username   TEXT    UNIQUE,
    first_name TEXT    NOT NULL,
    last_name  TEXT,
    created_at TEXT    NOT NULL,
    updated_at TEXT    NOT NULL,
    last_visit TEXT    NOT NULL
);


-- Table: user_setting
DROP TABLE IF EXISTS user_setting;

CREATE TABLE IF NOT EXISTS user_setting (
    user_id    INTEGER,
    type_id    INTEGER,
    value      TEXT,
    created_at TEXT,
    updated_at TEXT,
    CONSTRAINT user_setting_type_id PRIMARY KEY (
        user_id,
        type_id
    ),
    CONSTRAINT FK_user_setting_user FOREIGN KEY (
        user_id
    )
    REFERENCES user (id) ON DELETE CASCADE
                         ON UPDATE CASCADE
);


-- Table: user_setting_type
DROP TABLE IF EXISTS user_setting_type;

CREATE TABLE IF NOT EXISTS user_setting_type (
    id    TEXT PRIMARY KEY
               NOT NULL,
    about TEXT
);


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
