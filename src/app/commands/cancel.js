/**
 * Cancel last operation
 */
import { isInput, onInput } from '#scene'

export default async function(ctx) {
    if (ctx.chat.type !== 'private') return

    if (!isInput(ctx)) {
        return ctx.reply('Нет активной операции для отмены.')
    }

    onInput(ctx)
    return ctx.reply('Последняя операция была отменена.')
}
