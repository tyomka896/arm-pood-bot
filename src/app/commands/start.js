/**
 * Start command
 */
import help from './help.js'

export default async function(...agrs) {
    if (ctx.chat.type !== 'private') return

    return help(...agrs)
}
