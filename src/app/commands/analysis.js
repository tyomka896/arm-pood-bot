/**
 * Analysis command
 */
export default async function(ctx) {
    if (ctx.chat.type !== 'private') return

    return ctx.scene.enter('main', { act: 'analysis' } )
}
