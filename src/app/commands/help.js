/**
 * Help command
 */
import { getAuth } from '#controllers/userController.js'

export default async function(ctx) {
    if (ctx.chat.type !== 'private') return

    const auth = await getAuth(ctx)

    const rows = [
        '<b>Команды</b>\n',
        // '/search - поиск\n',
        '/analysis - анализ ПОДД\n',
        '/cancel - отмена последнего действия',
    ]

    if (auth.admin()) {
        // . . .
    }

    return ctx.reply(rows.join(''), { parse_mode: 'HTML' })
}
