/**
 * Main start scene
 */
import { Markup } from 'telegraf'
import { mainScene } from '#scene'

/** Keyboard */
export async function analysisKeyboard(ctx) {
    return [
        'Аналитика',
        Markup.inlineKeyboard([
            [ Markup.button.callback('Подписки', 'analysis-subscription') ],
        ])
    ]
}

/** Actions */
mainScene.action('analysis-subscription', async ctx => {
    ctx.answerCbQuery()

    return ctx.reply('Идет анализ подписок.')
})
