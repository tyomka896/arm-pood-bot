/**
 * Main start scene
 */
import { Markup } from 'telegraf'
import { mainScene } from '#scene'

/** Keyboard */
export async function searchKeyboard(ctx) {
    return [
        'Поиск',
        Markup.inlineKeyboard([
            [ Markup.button.callback('Подписка', 'search-subscription') ],
        ])
    ]
}

/** Actions */
mainScene.action('search-subscription', async ctx => {
    ctx.answerCbQuery()

    return ctx.reply('Поиск подписки по uuid.')
})
