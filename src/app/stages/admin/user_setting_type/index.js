/**
 * Main start scene
 */
import { UserSettingType } from '#models/user_setting_type.js'
import { onInput } from '#scene'

/** Keyboard */
export async function settingTypeKeyboard(ctx) {
    onInput(ctx, settingTypeName)

    return [ 'Введите идентификатор нового вида настроек.' ]
}

/** Messages */
export async function settingTypeName(ctx) {
    let value = ctx.message.text

    if (! value) {
        return ctx.reply('Вид настроек не может быть пустым.')
    }

    value = value.trim().replace(/\r?\n/g, '')

    if (value.length > 50) {
        return ctx.reply('Идентификатор не может превышать 50 символов.')
    }

    if (await UserSettingType.findByPk(value)) {
        return ctx.reply('Такой вид настроек уже существует.')
    }

    ctx.session.settingType = { id: value }

    onInput(ctx, settingTypeAbout)

    return ctx.reply(
        'Введите описание нового вида настроек.\n' +
        'Отправьте /empty, чтобы оставить пустым.'
    )
}

export async function settingTypeAbout(ctx) {
    const value = ctx.message.text

    if (value && value.length > 250) {
        return ctx.reply('Описание не должно превышать 250 символов.')
    }

    const { id } = ctx.session.settingType

    const settingType = await UserSettingType.create({
        id,
        about: value,
    })

    onInput(ctx)

    delete ctx.session.settingType

    return ctx.reply(
        `Новый вид настроек <i>${settingType.id}</i> успешно создан.`,
        { parse_mode: 'HTML' }
    )
}
