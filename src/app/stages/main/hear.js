/**
 * Handle user's raw input
 */

/**
 * Is input expected
 */
export function isInput(ctx) {
    if (ctx && ctx.constructor?.name !== 'Context') {
        throw Error('Invalid parameter: ctx.')
    }

    return Boolean(ctx.session.input)
}

/**
 * Called module file path
 */
function modulePath() {
    const stackFile = new Error().stack.split('\n')[3]

    const filePath = new RegExp('file:\/+(\/{1}.+[^:0-9]):[0-9]+:').exec(stackFile)

    if (! filePath) return null

    return filePath[1]
}

/**
 * Change input handler for the user
 */
export function onInput(ctx, method) {
    if (ctx && ctx.constructor?.name !== 'Context') {
        throw Error('Invalid parameter: ctx.')
    }

    if (! ctx.session) return ctx

    if (! method || (typeof method !== 'string' && typeof method !== 'function')) {
        return delete ctx.session.input
    }

    const path = modulePath()

    if (! path) return ctx

    ctx.session.input = {
        path: path,
        name: typeof method === 'function' ? method.name : method,
    }

    return ctx
}
