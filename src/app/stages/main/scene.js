/**
 * Main scene declaration
 */
import { Scenes } from 'telegraf'

export const mainScene = new Scenes.BaseScene('main')
