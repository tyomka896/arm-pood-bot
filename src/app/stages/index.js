/**
 * Main stage
 */
import { Scenes } from 'telegraf'

import { mainScene } from '#scene'
import help from '../commands/help.js'
import { searchKeyboard } from './user/search/index.js'
import { analysisKeyboard } from './user/analysis/index.js'
import { settingTypeKeyboard } from './admin/user_setting_type/index.js'

/** Actions */
mainScene.enter(async ctx => {
    switch (ctx.scene.state.act) {
        case 'search': return ctx.reply(...await searchKeyboard(ctx))
        case 'analysis': return ctx.reply(...await analysisKeyboard(ctx))
        case 'user_setting_type':
            return ctx.reply(...await settingTypeKeyboard(ctx))
        default: return help(ctx)
    }
})

/**
 * User's input handler
 */
mainScene.hears(/^([^\/]|\/empty).*/, async ctx => {
    if (! ctx.session.input) {
        return ctx.reply('Отправь /help для вывода полного списка команд.')
    }

    if (ctx.message.text === '/empty') {
        ctx.message.text = null
    }

    const { path, name } =  ctx.session.input

    const module = await import(path)

    if (module[name]) {
        return module[name](ctx)
    }
})

export const stage = new Scenes.Stage([ mainScene ])
