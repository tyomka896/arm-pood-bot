/**
 * Middleware for storing user's state
 */
import { Session } from '#models/session.js'

/** Local storage for quick access */
const store = {}

/**
 * Get session of the user
 */
async function getSession(ctx) {
    const keys = {
        user_id: ctx.from.id,
        chat_id: ctx.chat.id,
    }

    const state = await Session.findOne({ where: keys }) ||  await Session.create(keys)

    return JSON.parse(state.value)
}

/**
 * Update session of the user
 */
async function updateSession(ctx, state) {
    const keys = {
        user_id: ctx.from.id,
        chat_id: ctx.chat.id,
    }

    return await Session.update({ value: JSON.stringify(state) }, { where: keys })
}

export default async function(ctx, next) {
    const key = ctx.from && ctx.chat && `${ctx.from.id}|${ctx.chat.id}`

    if (!key) return next()

    ctx.session = store[key] || await getSession(ctx)

    return next().then(() => {
        store[key] = ctx.session
        updateSession(ctx, ctx.session)
    })
}
