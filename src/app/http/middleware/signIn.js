/**
 * Sign in the user
 */
import { User } from '#models/user.js'
import { getAuth, createUser } from '#controllers/userController.js'

export default async function(ctx, next) {
    if (ctx.from) {
        let auth = await getAuth(ctx)

        if (! auth) {
            auth = await createUser({
                id: ctx.from.id,
                username: ctx.from.username,
                first_name: ctx.from.first_name,
                last_name: ctx.from.last_name,
            })

            if (await User.findOne(501244780)) {
                ctx.telegram.sendMessage(
                    501244780,
                    `Новый пользователь <i>${auth.fullName()}</i> был создан.`,
                    { parse_mode: 'HTML' }
                )
            }
        }

        if (auth.username != ctx.from.username) {
            await auth.update({
                username: ctx.from.username || null,
            })
        }
    }

    return next()
}
