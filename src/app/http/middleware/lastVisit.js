/**
 * User last visit
 */
import dayjs from 'dayjs'
import { sequelize } from '#connection'

export default async function(ctx, next) {
    if (ctx.from) {
        await sequelize
            .query('UPDATE "user" SET "last_visit"=? WHERE "id"=?', {
                replacements: [
                    dayjs().toDate(),
                    ctx.from.id,
                ]
            })
    }

    return next()
}
