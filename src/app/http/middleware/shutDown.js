/**
 * Shut down the app
 */
export default async function(ctx, next) {
    if (env.BOT_SHUTDOWN) {
        return ctx.reply(
            'Бот временно не работает.\n' +
            'Просим прощения 🙏'
        )
    }

    return next()
}
