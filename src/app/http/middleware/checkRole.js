/**
 * Check if user can user the bot
 */
import { getAuth } from '#controllers/userController.js'

export default async function(ctx, next) {
    const auth = await getAuth(ctx)

    if (auth.user() || auth.admin()) return next()

    return ctx.reply('У вас нет доступа к боту, запрос отправлен.')
}