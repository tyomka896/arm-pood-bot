/**
 * Setting sequelize connection
 */
import path from 'path'
import { Sequelize } from 'sequelize'
import dayjs from 'dayjs'

function logging(query) {
    console.log(
        `[${dayjs().format('DD.MM.YYYY HH:mm:ss')}] ` +
        query.replace('Executing (default): ', '')
    )
}

/**
 * Database connection
 */
export const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: path.join(env.STORAGE_PATH, 'app/db.sqlite'),
    logging: env.DB_LOGGING ? logging : false,
    logQueryParameters: env.DB_LOGGING,
    timezone: '+00:00',
})
