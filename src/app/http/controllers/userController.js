/**
 * User controller
 */
import dayjs from 'dayjs'
import { User } from '#models/user.js'

/**
 * Auth info
 * @param {Context} ctx
 */
export async function getAuth(ctx) {
    if (ctx.constructor?.name !== 'Context') {
        throw Error('Invalid parameter: ctx.')
    }

    if (! ctx._auth) {
        ctx._auth = await User.findByPk(ctx.from.id)
    }

    return ctx._auth
}

/**
 * Create new user
 * @param {Object} data
 */
export async function createUser(data) {
    if (typeof data !== 'object') {
        throw Error('Invalid parameter: data.')
    }

    const model = await User.create({
        id: data.id,
        username: data.username,
        first_name: data.first_name,
        last_name: data.last_name,
        last_visit: dayjs().toDate(),
    })

    console.log(`New user '${model.fullName()}' (#${model.id}) was created!`)

    return model
}
