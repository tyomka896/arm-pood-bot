/**
 * User model
 */
import { Model, DataTypes } from 'sequelize'
import { Session } from './session.js'
import { sequelize } from '#connection'
import { UserSetting } from './user_setting.js'

/**
 * Extending model
 */
export class User extends Model {
    /**
     * If the user's status user
     */
    user() {
        return this.role === 'user'
    }

    /**
     * If the user's status admin
     */
    admin() {
        return this.role === 'admin'
    }

    /**
     * Link name from login or first and last names
     */
    linkName() {
        if (this.username) return `@${this.username}`

        return [
            this.last_name || '',
            this.first_name
        ]
            .filter(elem => elem)
            .join(' ')
    }

    /**
     * Concat last name, first name and login
     */
    fullName(login = true) {
        return [
            this.last_name || '',
            this.first_name,
            this.username && login ? `@${this.username}` : ''
        ]
            .filter(elem => elem)
            .join(' ')
    }

    /**
     * Get user setting
     * @param {string} key
     */
    async getItem(key) {
        if (! key) {
            throw Error('Invalid parameter: key.')
        }

        return await UserSetting.findOne({
            where: {
                user_id: this.id,
                type_id: key,
            },
        })
    }

    /**
     * Get user setting value
     * @param {string} key
     * @param {any} def
     */
    async getItemValue(key, def = null) {
        const item = await this.getItem(key)

        return item ? item.value : def
    }

    /**
     * Set user setting
     * @param {string} key
     * @param {any} value
     */
     async setItem(key, value) {
        if (! key) {
            throw Error('Invalid parameter: key.')
        }
        if (value === null || value === undefined) {
            throw Error('Invalid parameter: value.')
        }

        if (typeof value === 'boolean' || typeof value === 'number') {
            value = (+value).toString()
        }
        else if (typeof value === 'object' || Array.isArray(value)) {
            value = JSON.stringify(value)
        }

        const keyPair = { user_id: this.id, type_id: key }

        const setting = await UserSetting.findOne({ where: keyPair })

        return setting ? await setting.update({ value }) :
            await UserSetting.create({ ...keyPair, value })
    }
}

/**
 * Model structure
 */
User.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    utc: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
    },
    role: {
        type: DataTypes.TEXT,
        allowNull: false,
        defaultValue: 'none',
    },
    username: {
        type: DataTypes.TEXT,
    },
    first_name: {
        type: DataTypes.TEXT,
        allowNull: false,
    },
    last_name: {
        type: DataTypes.TEXT,
    },
    created_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    last_visit: {
        type: DataTypes.DATE,
        allowNull: false,
    }
}, {
    sequelize,
    tableName: 'user',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
})

/**
 * Get the sessions for the user.
 */
User.hasMany(Session, {
    foreignKey: 'user_id',
    as: 'Session'
})

/**
 * Get the user that owns the workout.
 */
UserSetting.belongsTo(User, {
    foreignKey: 'user_id',
    as: 'user',
})

/**
 * Get the workouts for the user.
 */
User.hasMany(UserSetting, {
    foreignKey: 'user_id',
    as: 'setting',
})
