/**
 * User setting type model
 */
import { Model, DataTypes } from 'sequelize'
import { sequelize } from '#connection'

/** User setting type enum */
export const SETTING_TYPE = {
    // . . .
}

/**
 * Extending model
 */
export class UserSettingType extends Model {  }

/**
 * Model structure
 */
UserSettingType.init({
    id: {
        type: DataTypes.STRING(50),
        primaryKey: true,
    },
    about: {
        type: DataTypes.STRING(250),
    },
}, {
    sequelize,
    tableName: 'user_setting_type',
    timestamps: false,
})
