/**
 * Starting point of the bot
 */
import './config/env.js'
import { Telegraf } from 'telegraf'
import { Bootstrap } from './app/bootstrap.js'
import agent from 'https-proxy-agent'

/** Creating */
const proxyAgent = env.PROXY_URL ?
    new agent.HttpsProxyAgent(env.PROXY_URL) :
    undefined

const bot = new Telegraf(
    env.BOT_TOKEN,
    {
        telegram: { agent: proxyAgent },
    }
)

/** Bootstrapping and launching bot */
await (await Bootstrap(bot)).launch()
