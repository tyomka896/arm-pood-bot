/**
 * App configuration
 */
import 'dotenv/config'
import path from 'path'

global.env = {
    BOT_TOKEN: process.env.BOT_TOKEN,
    BOT_SHUTDOWN: process.env.BOT_SHUTDOWN === 'true'||
        Boolean(parseInt(process.env.BOT_SHUTDOWN)),

    DB_LOGGING: process.env.DB_LOGGING === 'true'||
        Boolean(parseInt(process.env.DB_LOGGING)),

    PROXY_HOST: process.env.PROXY_HOST,
    PROXY_PORT: process.env.PROXY_PORT,
    PROXY_URL: process.env.PROXY_URL,

    STORAGE_PATH: path.resolve('src/storage')
}
